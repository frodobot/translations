#!/bin/sh

echo "Generating build.json"
node tools/build.js > build.json
echo "Build.json generated"

mkdir ~/.ssh/
echo -e "$(ssh-keyscan gitlab.com)" > ~/.ssh/known_hosts
echo "${SSH_PUSH_KEY}" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
git config user.email "bot@frodo.fun"
git config user.name "Frodo"
git remote remove ssh_origin || true
git remote add ssh_origin "git@gitlab.com:frodobot/translations.git"

git add build.json
git commit -m "[Deploy] Update build.json"
git push ssh_origin HEAD:master
