// This script can be used to easily generate a document that will compare two different languages translations

const fs = require('fs');

function exit(message) {
	console.error(message);
	process.exit(1);
}

function writeToFile(fileName, data) {
	fs.writeFileSync(fileName, data);
}

let build;
try {
	build = require('../build.json');
} catch (err) {
	exit('Please run "node tools/build.js > build.json" before running this script');
}

const fromLanguage = process.argv[2];
const targetLanguage = process.argv[3];
const fileName = process.argv[4];

if (!fromLanguage || !targetLanguage || !fileName) {
	exit('Usage: node makeCompareDocument.js <fromLanguage> <targetLanguage> <fileName>');
}

console.log(`Generating a comparing language document from ${fromLanguage} to ${targetLanguage}`);

const fromLanguageData = build[fromLanguage];
const targetLanguageData = build[targetLanguage];

if (!fromLanguageData || !targetLanguageData) {
	console.error(`Language ${fromLanguageData ? targetLanguage : fromLanguage} not found in build.json`);
	exit(`The current build.json only contains languages: ${Object.keys(build).map((lang) => `\n    - ${lang}`).join('')}`);
}

const date = new Date();
const dateString = date.toISOString().replace(/T/, ' ').replace(/\..+/, '').split(/ /).reverse().join(' ');
const output = [
	`## This file was automatically generated on ${dateString}`,
	`## It contains a list of all the strings that are different between ${fromLanguage} and ${targetLanguage}`,
	'## All lines that begin with "##" (like these ones) can be ignored as they are only comments',
	'## Translations that contain {{...}} are used as template values and need to be included in the translation',
	'##     e.g. In the live system, the translation "Hello {{name}}" will be displayed as "Hello John"',
	'## The translation lines are in the format:',
	'##     <key> = <fromTranslation> -> <targetTranslation>',
	'##     key: the key of the string in the language file (only needed for development and can be ignored)',
	'##     fromTranslation: the translation in the language file that is being compared',
	'##     targetTranslation: the translation in the language file that is being compared to',
	'', '',
];

for (const file of Object.keys(fromLanguageData)) {
	const fromFileData = fromLanguageData[file];
	const targetFileData = targetLanguageData[file];
	if (!fromFileData || !targetFileData) {
		exit(`File ${file} not found in ${fromFileData ? targetLanguage : targetLanguage}`);
	}

	const fromKeys = Object.keys(fromFileData);
	const targetKeys = Object.keys(targetFileData);
	if (!fromKeys.every((key, index) => key === targetKeys[index])) {
		console.error(`Keys in ${file} do not match between ${fromLanguage} and ${targetLanguage}`);
		exit('This could be caused by missing translations');
	}

	output.push(`## ${file}`);
	for (const key of fromKeys) {
		const fromValue = fromFileData[key];
		const targetValue = targetFileData[key];
		output.push(`${key}: "${fromValue}" -> "${targetValue}"`);
	}
	output.push('');
}

try {
	console.log(`Writing to ${fileName}`);
	writeToFile(fileName, output.join('\n'));
	console.log('Done');
} catch (err) {
	console.error(err);
	exit('Failed to write to file');
}
