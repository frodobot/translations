const fs = require('fs');
const path = require('path');

const translationsDirectory = path.join(__dirname, '..', 'translations');
const languageData = {};

const isTest = process.argv.includes('--test');
log('Processing language data');

const languages = getFilesInDirectory(translationsDirectory, 'No translations directory found');
languages.forEach((language) => {
	log(`Processing language ${language}`);
	languageData[language] = {};
	const languageDirectory = path.join(translationsDirectory, language);
	const files = getFilesInDirectory(languageDirectory, `No translations directory found for ${language}`);
	files.forEach((file) => {
		log(` -> Processing file ${file}`);
		const filePath = path.join(languageDirectory, file);
		const fileData = fs.readFileSync(filePath, 'utf8');
		const fileDataJson = JSON.parse(fileData);
		languageData[language][file.replace(/\.json$/, '')] = fileDataJson;
	});
});

if (!isTest) process.stdout.write(JSON.stringify(languageData));
log(`Language data:\n${JSON.stringify(languageData)}`);
log ('Done');

function getFilesInDirectory(directory, errorMessage) {
	try {
		return fs.readdirSync(directory);
	} catch (err) {
		console.error(errorMessage);
		console.error(err);
		process.exit(1);
	}
}

function log(message) {
	if (isTest) console.log(message);
}
