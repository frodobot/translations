![Localized](https://badges.crowdin.net/frodobot/localized.svg)

## Frodo Translations

- This repository manages all translations of the frodo bot
- Translations can be edited and contributed to on https://crwd.in/frodobot
- Once you make a change/add a languages, the changes will be imported into the latest mr in this repo, which when checked by an owner, will be merged and automatically put into frodo
